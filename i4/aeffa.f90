program aeffa
    use, intrinsic :: iso_c_binding
    use module_hdf5
    implicit none
    integer, parameter :: nx = 3, ny = 2, nz = 3
    integer :: ix, iy, iz, kkk
    double precision, dimension(nx, ny, nz) :: f
    double precision, dimension(nx, ny, nz, 2) :: g  ! on teste comme si c'était 2 tableaux 3D ?
    double precision, dimension(nx, ny) :: tab2d  !
    double precision, dimension(nx) :: tab1d  !
    double precision, dimension(nx * ny * nz) :: tabreal1, tabreal2
    DOUBLE PRECISION, DIMENSION(:), allocatable :: double_member
    integer :: ok
    
    print *, " nx = ", nx, " ny = ", ny, " nz = ", nz, " ", nx * ny * nz
    print *, " init tableau 1d"
    do ix = 1, nx * ny * nz
        tabreal1(ix) = 1.0d0 * ix
        if (nx * ny * nz < 100) then
            write(*, fmt = "(f6.2)", advance = "no") tabreal1(ix)
        end if
    enddo
    print *
    
    print *, " init tableau 3d à partir du tableau 1d "
    f = reshape(tabreal1, (/nx, ny, nz/))
    do iz = 1, nz  ! on regarde par plan d'élévation
        if (nx * ny * nz < 100) then
            print *, "plan z = ", iz
        end if
        do ix = 1, nx  ! pour chaque ligne, on écrit les colonnes
            !write(*, fmt="(f6.2)", advance="no") f(ix,:,iz)
            if (nx * ny * nz < 100) then
                print *, f(ix, :, iz)
            end if
        end do
    enddo
    
    print *, " changement de profil 3d -> 1d"
    tabreal2 = pack(f, mask = .true.)
    if (nx * ny * nz < 100) then
        do ix = 1, nx * ny * nz
            write(*, fmt = "(f6.2)", advance = "no") tabreal2(ix)
        enddo
        print *
    end if
    
    print *, "  on sauve le tableau 3d en HDF5"
    allocate(double_member(nx * ny * nz), stat = ok)
    if (ok /= 0) then
        print *, "Erreur allocation impossible ", nx * ny * nz
        stop 123
    end if
    double_member = pack(f, mask = .true.)
    CALL saveToHDF5(double_member, [size(f, 1), size(f, 2), size(f, 3)], "tab3d.h5", "mondataset")
    print *, "fichier tab3d.h5 crée, avec le dataset /dimensions et le dataset /mondataset"
    
    deallocate(double_member)
    
    print *, "  on sauve le tableau 4d en HDF5"
    ! pour test, on duplique 2 fois le tableaux 3d
    g(:, :, :, 1) = f(:, :, :)
    g(:, :, :, 2) = f(:, :, :)
    
    allocate(double_member(nx * ny * nz * 2), stat = ok)
    if (ok /= 0) then
        print *, "Erreur allocation impossible ", nx * ny * nz * 2
        stop 123
    end if
    double_member = pack(g, mask = .true.)
    CALL saveToHDF5(double_member, [size(g, 1), size(g, 2), size(g, 3), size(g, 4)], &
            & "tab4d.h5", "mondataset")
    print *, "fichier tab4d.h5 crée, avec le dataset /dimensions et le dataset /mondataset"
    
    deallocate(double_member)
    
    ! on sauve un tableau 1d
    print *, "  test avec un tableau 1d en HDF5"
    allocate(double_member(nx), stat = ok)
    if (ok /= 0) then
        print *, "Erreur allocation impossible ", nx
        stop 124
    end if
    do ix = 1, nx
        double_member(ix) = 1.0d0 * ix
        print *, double_member(ix)
    end do
    CALL saveToHDF5(double_member, [size(double_member, 1)], "tab1d.h5", "mondataset")
    print *, "fichier tab1d.h5 crée, avec le dataset /dimensions et le dataset /mondataset"
    
    deallocate(double_member)
    
    ! on sauve un tableau 2d
    print *, "  test avec un tableau 2d en HDF5"
    do ix = 1, nx
        do iy = 1, ny
            tab2d(ix, iy) = 1.0d0 * ix
            print *, tab2d(ix, iy)
        end do
    end do
    double_member = pack(tab2d, mask = .true.)
    CALL saveToHDF5(double_member, [size(tab2d, 1), size(tab2d, 2)], "tab2d.h5", "mondataset")
    print *, "fichier tab2d.h5 crée, avec le dataset /dimensions et le dataset /mondataset"
    
    deallocate(double_member)

end program aeffa
