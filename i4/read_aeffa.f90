! Created by me on 15/05/23.

program read_aeffa
    use iso_fortran_env
    use, intrinsic :: iso_c_binding
    use module_hdf5

    implicit none
    integer :: ix,iy,iz,nx,ny,nz
    !real  :: f(10,10,10)
    integer :: ii, ndims
    
    DOUBLE PRECISION, DIMENSION(:), allocatable :: data_out
    DOUBLE PRECISION, DIMENSION(:,:), allocatable :: tab2d
    DOUBLE PRECISION, DIMENSION(:,:,:), allocatable :: f
    INTEGER, DIMENSION(:), allocatable :: tab_dims   !  he dataset
    integer :: rang
    DOUBLE PRECISION :: dmin, dmax
    
    character(len=255) :: nomfich
    print *, "Name of file (add "" in case of name with /) "
    read *, nomfich
    print *, nomfich
    print *, "nombre de dimensions "
    read *, ndims
    print *, ndims
#ifdef OLD
    nx=10
    ny=10
    nz=10
    
    open(unit=12, file ='aeffa.bin', form = 'unformatted', status='old', action="read")
    do ix=1,nx
        do iy=1,ny
                read(unit=12) f(ix,iy,:)
                print *, ix, iy, iz, f(ix,iy,:)
        enddo
    enddo
    close(12)
#endif

    ! lecture d'un fichier hdf5 produit par le code aeffa, data_out est toujours un tableau 1d
    call readHDF5(trim(nomfich), "mondataset", data_out, tab_dims)
    ! on cherche le max et le min des données
    dmin = data_out(1)
    dmax = data_out(1)
        do ix = 1, size(data_out, 1)  ! pour chaque ligne, on écrit les colonnes
            if (dmin > data_out(ix)) dmin = data_out(ix)
            if (dmax < data_out(ix)) dmax = data_out(ix)
    enddo
    print *, " min lu ", dmin, " max lu ", dmax
    
    print *, " on verifie en creant à nouveau un fichier hdf5 avec les données lues"
    rang = rank(data_out)
    print *, " rank ", rank(data_out), " size ", size(data_out)
    select case (size(tab_dims))
    case (1)
        CALL saveToHDF5(data_out, [size(data_out)], "new_" // nomfich, "mondataset")
    case (2)
        nx = tab_dims(1)
        ny = tab_dims(2)
        CALL saveToHDF5(data_out, [nx, ny], "new_" // nomfich, "mondataset")
    case (3)
        nx = tab_dims(1)
        ny = tab_dims(2)
        nz = tab_dims(3)
        f = reshape(data_out, (/nx, ny, nz/))
        !allocate(f(nx,ny,nz))
        do iz = 1, nz  ! on regarde par plan d'élévation
            if (nx * ny * nz < 100) then
                print *, "plan z = ", iz
            end if
            do ix = 1, 2  ! pour chaque ligne, on écrit les colonnes
                !write(*, fmt="(f6.2)", advance="no") f(ix,:,iz)
                if (nx * ny * nz < 100) then
                    print *, f(ix, :, iz)
                end if
            end do
        enddo
        CALL saveToHDF5(data_out, [nx, ny, nz], "new_" // nomfich, "mondataset")
    case (4)
        print *, " TODO !!!"
    case default
        print *, __FILE__, ":", __LINE__, " ", " *** Error rank ", size(tab_dims), " missing code "
        stop 128
    end select
    
    print *, "fichier new_", nomfich, " crée, avec le dataset /dimensions et le dataset /mondataset"
    
    deallocate(data_out)  !!! Ne pas oublier de desallouer si on refait un appel à readHDF5 !!!!

end program read_aeffa