# librairie de lecture/écriture de fichiers HDF5 en fortran 90 pour le code de fluo

Source https://hdfgroup.github.io/hdf5/

## Compilation
Sur Ubuntu 20, le CMakeLists.txt ne détecte pas correctement la lib HDF5,
il faut utiliser le script build.sh pour compiler sur Ubuntu 
(sur Fedora, il faudra sans doute modifer les chemins), ou le makefile.


## Test d'utilisation en écriture
On utilise le programme Fortran **aeffa** qui crée les fichier HDF5 pour des tableaux 1d à 4d.
Ces fichiers sont facilement exploitables sous Matlab, par exemple, avec
le script **visu_binaire.m**

```
./aeffa
 nx =            3  ny =            2  nz =            3            18
  init tableau 1d
  1.00  2.00  3.00  4.00  5.00  6.00  7.00  8.00  9.00 10.00 11.00 12.00 13.00 14.00 15.00 16.00 17.00 18.00
  init tableau 3d à partir du tableau 1d 
 plan z =            1
   1.0000000000000000        4.0000000000000000     
   2.0000000000000000        5.0000000000000000     
   3.0000000000000000        6.0000000000000000     
 plan z =            2
   7.0000000000000000        10.000000000000000     
   8.0000000000000000        11.000000000000000     
   9.0000000000000000        12.000000000000000     
 plan z =            3
   13.000000000000000        16.000000000000000     
   14.000000000000000        17.000000000000000     
   15.000000000000000        18.000000000000000     
  changement de profil 3d -> 1d
  1.00  2.00  3.00  4.00  5.00  6.00  7.00  8.00  9.00 10.00 11.00 12.00 13.00 14.00 15.00 16.00 17.00 18.00
   on sauve le tableau 3d en HDF5
 fichier tab3d.h5 crée, avec le dataset /dimensions et le dataset /mondataset
   on sauve le tableau 4d en HDF5
 fichier tab4d.h5 crée, avec le dataset /dimensions et le dataset /mondataset
```

## Utilisation en lecture
on utilise le programme Fortran **read_aeffa** qui attend deux paramètres, le nom du fichier hdf5 (mettre des "" si
il y a des /) et le nombre de dimensions. Il suffit de tout mettre dans un fichier IN
```
me@nessie:~/projets/affichage/runs$ ../cmake-build-debug/read_aeffa < IN
 Name of file (add " in case of name with /)
 tab3d.h5

 nombre de dimensions
           3
 /home/me/projets/affichage/mod_hdf5.f90:         226   opening dataset dimensio
 /home/me/projets/affichage/mod_hdf5.f90:         234   rank            1
 /home/me/projets/affichage/mod_hdf5.f90:         240   dims
 /home/me/projets/affichage/mod_hdf5.f90:         253   tab_dims            3
 /home/me/projets/affichage/mod_hdf5.f90:         271   opening dataset mondatas
 /home/me/projets/affichage/mod_hdf5.f90:         277   rang            1
 /home/me/projets/affichage/mod_hdf5.f90:         283   dims
  on verifie en creant à nouveau un fichier hdf5 avec les données lues
  rank            1  size           18
 plan z =            1
   1.0000000000000000        4.0000000000000000
   2.0000000000000000        5.0000000000000000
 plan z =            2
   7.0000000000000000        10.000000000000000
   8.0000000000000000        11.000000000000000
 plan z =            3
   13.000000000000000        16.000000000000000
   14.000000000000000        17.000000000000000
 /home/me/projets/affichage/mod_hdf5.f90:         142  *** writing file mondatas
  data    1.0000000000000000        2.0000000000000000        3.0000000000000000
000000000        9.0000000000000000        10.000000000000000        11.00000000
.000000000000000        17.000000000000000        18.000000000000000
  data_dims                    18
 fichier new_tab3d.h5

```
ici on lit le fichier crée précédemment mais on peut aussi utiliser un 
fichier crée par matlab. Par exemple:
```
load("/home/me/Downloads/bathtub_0107_01.mat")
h5create('bathtub_0107_01.h5', '/dimensions', length(size(instance)), 'Datatype','int32'); % taille du tableau
h5write('bathtub_0107_01.h5', '/dimensions', size(instance)); % le tbleau des dimensions
h5create('bathtub_0107_01.h5', '/mondataset', size(instance), 'Datatype','double')
h5write('bathtub_0107_01.h5', '/mondataset', instance)
h5disp('bathtub_0107_01.h5');
```
qui donne:
```
HDF5 bathtub_0107_01.h5 
Group '/' 
    Dataset 'dimensions' 
        Size:  3
        MaxSize:  3
        Datatype:   H5T_IEEE_F64LE (double)
        ChunkSize:  []
        Filters:  none
        FillValue:  0.000000
    Dataset 'mondataset' 
        Size:  224x224x224
        MaxSize:  224x224x224
        Datatype:   H5T_IEEE_F64LE (double)
        ChunkSize:  []
        Filters:  none
        FillValue:  0.000000
```

à comparer avec:

```
HDF5 "new_bathtub_0107_01.h5" {
GROUP "/" {
   DATASET "dimensions" {
      DATATYPE  H5T_STD_I32LE
      DATASPACE  SIMPLE { ( 3 ) / ( 3 ) }
      DATA {
      (0): 224, 224, 224
      }
   }
   DATASET "mondataset" {
      DATATYPE  H5T_IEEE_F64LE
      DATASPACE  SIMPLE { ( 11239424 ) / ( 11239424 ) }
      DATA {
      (0): 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
      (22): 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
      (44): 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
      (66): 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,

```

## Tests
### Création d'un fichier HDF5 avec Matlab, lecture avec le module Fortran, et re-écriture d'un fichier HDF5 avec le module Fortran
- [ ] à partir d'un fichier .mat, création du fichier .h5 avec le script **cree_hdf5.m**
- [ ] on lance dans un terminal, le script 
```
rm bathtub_0107_01.h5 
matlab -nosplash -r cree_hdf5 
```
Le fichier **bathtub_0107_01.h5** est maintenant créé
- [ ] dans un nouveau terminal, on lance le programme **./read_aeffa** qui va lire le fichier HDF5 et en créer un 
nouveau (à partir des routines Fortran)
```
me@nessie:~/projets/affichage/runs$ ../read_aeffa < IN
 Name of file (add " in case of name with /)
 bathtub_0107_01.h5

 nombre de dimensions
           3
  on verifie en creant à nouveau un fichier hdf5 avec les données lues
  rank            1  size     11239424
 fichier new_bathtub_0107_01.h5
                                                                    crée, avec le dataset /dimensions et le dataset /mondataset
```
le fichier IN contient simplement le nom complet (avec le chemin) du fichier à relire.
Le nouveau fichier créé doit contenir les mêmes données (même s'il n'est pas arrangé de la même façon)
Pour le vérifier, on va lancer le script matlab pour afficher l'objet :
```
matlab -nosplash -r verif_hdf5

```

## Utilisation pour des matrices comme global_matrix.dat
Cette matrice est initialement stockée dans un fichier texte, sous la forme de 4 colonnes:
```angular2html
ligne colonne partie réelle partie imaginaire
```

On va la stocker dans un ficiher HDF5, avec un dataset par colonne, et un dataset qui contient les deux dimensions N et NNZ, ainsi que des attributs contenant la description de la matrice

Pour cela, il suffit, dans le code Fortran, d'appeler:
```fortran
    CALL saveGlobalMatrixToHDF5(filename, N, NNZ, tab_rows, tab_cols, tab_re_mat, tab_im_mat)
```

Pour les attributs, il suffit de créer un fichier contenant au plus 4 lignes de 80 caractères, par exemple:
```text
Pb_medium_30GHz
crée le 20/10/2023
cas test pour mesocentre
ceci est un commentaire libre etc... etc...
```

puis de lancer l'exécutable, qui va demander le nom du fichier à lire.
Les attributs sont stockés dans une variable appelée attr_long
```json
h5dump -n 1 global_matrix.h5
 HDF5 "global_matrix.h5" {
 FILE_CONTENTS {
 group      /
 dataset    /cols
 dataset    /dimensions
 attribute  /dimensions/attr_long
 dataset    /im_mat
 dataset    /re_mat
 dataset    /rows
 }
 }
```
```json
 h5dump -a "/dimensions/attr_long" global_matrix.h5
 HDF5 "global_matrix.h5" {
 ATTRIBUTE "attr_long" {
 DATATYPE  H5T_STRING {
 STRSIZE 80;
 STRPAD H5T_STR_SPACEPAD;
 CSET H5T_CSET_ASCII;
 CTYPE H5T_C_S1;
 }
 DATASPACE  SIMPLE { ( 4 ) / ( 4 ) }
 DATA {
 (0): "CONTEXTE                                                                        ",
 (1): "Pb_medium_30GHz                                                                 ",
 (2): "ATTR1                                                                           ",
 (3): "val1                                                                            "
 }
 }
 }

```
Si le fichier n'existe pas, le programme n'ajoute pas de metadonnées.

# Exécution sur le mesocentre
```shell
module load userspace 
module load gcc/11.2.0 
CC=gcc FC=gfortran ~/cmake-3.27.6/bin/cmake -DHDF5_ROOT=/home/ghenry/LIBS/HDF5-1.12.0-gcc11 -DCMAKE_BUILD_TYPE=Debug ..
make VERBOSE=1 2>&1 |& tee MAKE.LOG
mkdir -p MATRIX_STORAGE TEMP_FILE
```
TEMP_FILE doit contenir le fichier global_matrix.dat
Mettre à jour solve.sh pour appeler le bon exécutable
