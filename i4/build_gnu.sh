#/bin/bash


# ce script fonctionne sur Ubuntu 20 et sur tramel

# pour tramel
HDF5_DIR=/usr/local/hdf5-1.10.10
LIB1=$HDF5_DIR/lib/
LIB2=$HDF5_DIR/lib/
INC1=$HDF5_DIR/include/

# pour ubuntu 20
LIB1=/usr/lib64/gfortran/modules
LIB2=/usr/lib/x86_64-linux-gnu/hdf5/serial
INC1=/usr/include/hdf5/serial

export LIB1; export LIB2; export INC1
export PATH=/usr/local/bin:$PATH

gfortran -v

gfortran -cpp -Wall -Wextra -Wimplicit-interface -fPIC -fmax-errors=1 -g -fcheck=all -fbacktrace \
	-I$INC1 module_hdf5.f90 aeffa.f90 \
        -fintrinsic-modules-path $LIB1 \
	-Wl,-rpath=/usr/local/lib64/ \
	-Wl,-rpath=$LIB2 \
        -L$LIB2  -lhdf5_fortran \
	-o aeffa

gfortran -cpp -Wall -Wextra -Wimplicit-interface -fPIC -fmax-errors=1 -g -fcheck=all -fbacktrace \
	-I$INC1 module_hdf5.f90 read_aeffa.f90 \
        -fintrinsic-modules-path $LIB1 \
	-Wl,-rpath=/usr/local/lib64/ \
	-Wl,-rpath=$LIB2 \
        -L$LIB2  -lhdf5_fortran \
	-o read_aeffa



gfortran -cpp -I$INC1  -c hdf5_wrapper.f90
gfortran -cpp -I$INC1  -c module_hdf5.f90
gfortran -cpp -Wall -Wextra -Wimplicit-interface -fPIC -fmax-errors=1 -g -fcheck=all -fbacktrace \
	-I$INC1 module_hdf5.f90 hdf5_wrapper.f90 write_global_matrix.f90 \
        -fintrinsic-modules-path $LIB1 \
	-Wl,-rpath=/usr/local/lib64/ \
	-Wl,-rpath=$LIB2 \
        -L$LIB2  -lhdf5_fortran \
	-o write_global_matrix

gfortran -cpp -Wall -Wextra -Wimplicit-interface -fPIC -fmax-errors=1 -g -fcheck=all -fbacktrace \
	-I$INC1 module_hdf5.f90 hdf5_wrapper.f90 read_global_matrix.f90 \
        -fintrinsic-modules-path $LIB1 \
	-Wl,-rpath=/usr/local/lib64/ \
	-Wl,-rpath=$LIB2 \
        -L$LIB2  -lhdf5_fortran \
	-o read_global_matrix

