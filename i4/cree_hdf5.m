%% GH 25/11/23
% 
% choisir un fichier .mat contenant un tableau 2d ou 3d
% bathtub_0107_01: fichier trouvé sur https://github.com/zeaggler/ModelNet_Mat2PNG

% ne pas oublier addpath pour le dossier ou se trouve le script imshow3D.m
addpath("~/MATLAB Add-Ons/")

chemin=('/home/me/Downloads/');
chemin=('/net/10.48.10.46/Public/Kevin/Data/2020-11-06/');
nom=('bathtub_0107_01');
nom=('acquisition_06-11-2020_17-48-32');
fich=fullfile(chemin, strcat(nom, '.mat'));
load(fich)

%%
%imshow3D(instance, [], 1);
imshow3D(data, [], 1);
colorbar
colormap parula

%% on teste avec un truc simple
% nom=('test_magic_5');
% testdata=magic(5);

%% 1d
% instance=[1,2,3,4,5]
% nom=('test_1d');

nomh5=fullfile('/home/me/projets/affichage/runs', strcat(nom, '.h5'));


% cree le prmeier dataset qui contient les dimensions des données
h5create(nomh5, '/dimensions', length(size(instance)), 'Datatype','int32'); % taille du tableau
h5write(nomh5, '/dimensions', size(instance)); % le tbleau des dimensions


% le dataset des données
h5create(nomh5, '/mondataset', size(instance), 'Datatype','double')
h5write(nomh5, '/mondataset', instance)

% vérification
h5disp(nomh5);

imshow3D(instance, [], 1);
colorbar
colormap parula
