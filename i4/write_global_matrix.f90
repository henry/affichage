program write_global_matrix
    use, intrinsic :: iso_c_binding
    use iso_fortran_env, only : int32, int64
    use module_hdf5
    use hdf5_wrapper

    implicit none
    integer :: ix, iy, iz, kkk
    REAL*8 RE_MAT, IM_MAT, RE_RHS, IM_RHS
    INTEGER(kind = int32), DIMENSION(:), allocatable :: tab_rows, tab_cols
    DOUBLE PRECISION, DIMENSION(:), allocatable :: double_member, tab_re_mat, tab_im_mat
    integer :: ok
    CHARACTER(LEN = *), parameter :: filename = "MATRIX_STORAGE/global_matrix.h5"
    integer :: N, NNZ
    integer(kind = int32) :: I8, ROW, COL
    integer(kind = int32) :: i1, i2
    integer :: i3, i4
    integer(kind = int64) :: i5
    integer :: i
    real*8 frequency

    integer(hid_t) :: ifile
    INTEGER :: error
    character ofile*32
    integer :: ounit

    N = 2
    NNZ = 1

    ! on commence par ajouter les attributs à un fichier vide
    call hdf5_init()
    call hdf5_create_file(filename)
    call hdf5_open_file(filename, ifile)
    call hdf5_write_data(ifile, '/description', 4)  ! 3 pour dire qu'on a mis 3 attributs
    frequency = 1.0e9
    call hdf5_write_attribute(ifile, '/description', 'attr_name1', 1.23)
    call hdf5_write_attribute(ifile, '/description', 'attr_name2', 'EF_3D_NEW')
    call hdf5_write_attribute(ifile, '/description', 'contexte', 'petite matrice')
    call hdf5_write_attribute(ifile, '/description', 'provider', 'tortel')

    open(unit = 123, file = "TEMP_FILE/global_matrix.dat")
    print *, "lit le fichier ", "TEMP_FILE/global_matrix.dat"
    READ(123, *) N
    print *, "N = ", N
    READ(123, *) NNZ
    print *, "NNZ = ", NNZ
    
    ! on sauve un tableau 1d
    print *, "  lecture fichiers global_matrix et rhs"
    allocate(tab_re_mat(NNZ), stat = ok)
    if (ok /= 0) then
        print *, "Erreur tab_re_mat allocation impossible ", NNZ
        stop 124
    end if
    allocate(tab_im_mat(NNZ), stat = ok)
    if (ok /= 0) then
        print *, "Erreur tab_im_mat allocation impossible ", NNZ
        stop 125
    end if
    allocate(tab_cols(NNZ), stat = ok)
    if (ok /= 0) then
        print *, "Erreur tab_cols allocation impossible ", NNZ
        stop 126
    end if
    allocate(tab_rows(NNZ), stat = ok)
    if (ok /= 0) then
        print *, "Erreur tab_rows allocation impossible ", NNZ
        stop 126
    end if
    DO I8 = 1, NNZ
        READ(123, *) row, col, re_mat, im_mat
        tab_rows(I8) = row
        tab_cols(I8) = col
        tab_re_mat(I8) = re_mat
        tab_im_mat(I8) = im_mat
    END DO
    close(123)

    CALL saveGlobalMatrixToHDF5(ifile, N, NNZ, tab_rows, tab_cols, tab_re_mat, tab_im_mat)
    call hdf5_close_file(ifile)
    call hdf5_finalize()

    if (iargc() .gt. 0) then
        call getarg(1, ofile)
        print *, "on ecrit la matrice au format matrix market"
        ounit = 9
        open(unit = ounit, file = ofile)
        print *, 'Writing header and data...'
        write(ounit, '(A)') "%%MatrixMarket matrix coordinate  complex     general"
        write(ounit, *) N, N, NNZ
        DO I8 = 1, NNZ
            write(ounit, *) tab_rows(I8), tab_cols(I8), tab_re_mat(I8), tab_im_mat(I8)
        END DO
        close(9)
        print *, 'Done writing to file: ', ofile
    else
        print *, 'si vous voulez du MatrixMarket, ajouter le nom de fichier en second argument', iargc()
    endif
    deallocate(tab_rows)
    deallocate(tab_cols)
    deallocate(tab_re_mat)
    deallocate(tab_im_mat)

!    print *, "on lance le script solve.sh"
!
!    call execute_command_line ("bash ./solve.sh", exitstat = i)
!    print *, "Exit status of external_prog.exe was ", i
!    if (i .ne. 0) then
!        print *, "ERROR"
!    end if

end program write_global_matrix

