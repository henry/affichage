!================================================================
! Created by me on 16/05/23.
! Henry GERARD
! ce fichier contient des routines pour stocker un tableau au format HDF5
! avec des metadonnées, et une routine pour le relire
! source: les exemples HDF5 sur le site officiel: https://portal.hdfgroup.org/display/HDF5/Examples+by+API
! https://raw.githubusercontent.com/HDFGroup/hdf5/develop/fortran/examples/h5_crtdat.f90
! et
! https://raw.githubusercontent.com/HDFGroup/hdf5/develop/fortran/examples/h5_rdwt.f90
! et pour la partie compression
! https://bitbucket.hdfgroup.org/projects/HDFFV/repos/hdf5-examples/browse/1_10/FORTRAN/H5D/h5ex_d_gzip.f90
! https://stackoverflow.com/questions/35874881/reading-an-array-of-unknown-length-from-an-hdf-file-in-fortran
! https://forum.hdfgroup.org/t/write-variable-length-utf8-string-attribute-with-fortran-api/10336
!================================================================

! Exemple simple pour une matrice 3*2*3, sérialisée avec reshape
!plan z =            1
!1.0000000000000000        4.0000000000000000
!2.0000000000000000        5.0000000000000000
!3.0000000000000000        6.0000000000000000
!plan z =            2
!7.0000000000000000        10.000000000000000
!8.0000000000000000        11.000000000000000
!9.0000000000000000        12.000000000000000
!plan z =            3
!13.000000000000000        16.000000000000000
!14.000000000000000        17.000000000000000
!15.000000000000000        18.000000000000000
! qui donne
!HDF5 "tab3d.h5" {
!GROUP "/" {
!DATASET "dimensions" {
!DATATYPE  H5T_STD_I32LE
!DATASPACE  SIMPLE { ( 3 ) / ( 3 ) }
!DATA {
!(0): 3, 2, 3
!}
!}
!DATASET "mondataset" {
!DATATYPE  H5T_IEEE_F64LE
!DATASPACE  SIMPLE { ( 18 ) / ( 18 ) }
!DATA {
!(0): 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18
!}
!}
!}
!}

module module_hdf5
    USE ISO_C_BINDING
    use iso_fortran_env, only : int32, int64, real64
    
    USE HDF5 ! This module contains all necessary modules
    
    implicit none
    integer, parameter :: dp = real64
    CHARACTER(LEN = *), parameter :: FILE_ATTRIBUTES = "simu_attr.txt"  ! voir format particulier ici
    integer, parameter :: NBATTR = 4
    integer, parameter :: LGATTR = 20
    integer, parameter :: LGMAX = 80
    CHARACTER(LEN=9), PARAMETER :: aname = "attr_long"   ! Attribute name
    ! ligne impaire: nom de l'attribut
    ! ligne paire: valeur de l'attribut
    ! Exemple:
    ! CONTEXTE
    ! Pb_medium_30GHz
contains
    subroutine help()
        print *, "HDF5 help FIXME"
    endsubroutine help
    
    subroutine writerealbigdataset(file_id, NNZ, dsetname, rang, double_member)
        INTEGER(HID_T) :: file_id       ! File identifier
        INTEGER, intent(in) :: NNZ
        CHARACTER(LEN = *), intent(in) :: dsetname ! Dataset name
        INTEGER, intent(in) :: rang
        real(kind = dp), DIMENSION(:), allocatable, intent(in) :: double_member
        
        INTEGER :: error ! Error flag
        LOGICAL :: avail
        INTEGER(HID_T) :: dcpl ! Handles
        INTEGER :: filter_info
        INTEGER :: filter_info_both
        INTEGER(HSIZE_T), DIMENSION(1) :: chunk ! chunk dimensions
        INTEGER(HID_T) :: dset_id       ! Dataset identifier
        INTEGER(HID_T) :: dspace_id     ! Dataspace identifier
        INTEGER(HSIZE_T), DIMENSION(1) :: data_dims
        INTEGER(HSIZE_T), DIMENSION(1) :: dims ! Dataset dimensions
        INTEGER :: dimsize ! Size of the dataset
        
        dimsize = NNZ
        dims = (/dimsize/)
        data_dims(1) = dimsize
        
        !
        !  Check if gzip compression is available and can be used for both
        !  compression and decompression.  Normally we do not perform error
        !  checking in these examples for the sake of clarity, but in this
        !  case we will make an exception because this filter is an
        !  optional part of the hdf5 library.
        !
        CALL h5zfilter_avail_f(H5Z_FILTER_DEFLATE_F, avail, error)
        
        IF (.NOT.avail) THEN
            WRITE(*, '("gzip filter not available.",/)')
            STOP
        ENDIF
        CALL h5zget_filter_info_f(H5Z_FILTER_DEFLATE_F, filter_info, error)
        
        filter_info_both = IOR(H5Z_FILTER_ENCODE_ENABLED_F, H5Z_FILTER_DECODE_ENABLED_F)
        IF (filter_info /= filter_info_both) THEN
            WRITE(*, '("gzip filter not available for encoding and decoding.",/)')
            STOP
        ENDIF
        
        !
        ! Create the dataspace.
        !
        CALL h5screate_simple_f(rang, dims, dspace_id, error)
        !
        
        ! on ne compresse qu'à partir d'une certaine taille, et on utilise des bouts de taille différentes
        ! suvant la grosseur du tableau
        if ((dimsize > 500) .and. (dimsize < 100000)) then
            chunk = (/200/)
        elseif (dimsize >= 100000) then
            chunk = (/1000/)
        else
            chunk = (/0/) ! no chunk
        end if
        !
        ! Create the dataset creation property list, add the gzip
        ! compression filter and set the chunk size.
        !
        CALL h5pcreate_f(H5P_DATASET_CREATE_F, dcpl, error)
        if (chunk(1) > 0) then
            CALL h5pset_deflate_f(dcpl, 9, error)
            CALL h5pset_chunk_f(dcpl, rang, chunk, error)
        end if
        ! Create the dataset with with default properties.
        !
        CALL h5dcreate_f(file_id, dsetname, h5kind_to_type(dp, H5_REAL_KIND), dspace_id, &
                dset_id, error, dcpl)
        
        data_dims(1) = size(double_member)
        !
        ! Write data by fields in the datatype. Fields order is not important.
        !
        CALL h5dwrite_f(dset_id, h5kind_to_type(dp, H5_REAL_KIND), double_member, data_dims, error)
        IF (error<0) THEN
            print *, __FILE__, ":", __LINE__, " *** Error writing dataset " // dsetname
            return
        else
#ifdef DEBUG
            print *, __FILE__, ":", __LINE__, " *** writing dataset " // dsetname
            !print *, " data ", double_member
            print *, " data_dims ", data_dims
#endif
        ENDIF
        
        CALL h5pclose_f(dcpl, error)
        !
        ! End access to the dataset and release resources used by it.
        !
        CALL h5dclose_f(dset_id, error)
        
        !
        ! Terminate access to the data space.
        !
        CALL h5sclose_f(dspace_id, error)
#ifdef DEBUG
            print *, __FILE__, ":", __LINE__, " *** fin dataset " // dsetname
#endif
    
    endsubroutine writerealbigdataset
    
    subroutine writeintbigdataset(file_id, NNZ, dsetname, rang, double_member)
        INTEGER(HID_T) :: file_id       ! File identifier
        INTEGER, intent(in) :: NNZ
        CHARACTER(LEN = *), intent(in) :: dsetname ! Dataset name
        INTEGER, intent(in) :: rang
        INTEGER(kind=int64), DIMENSION(:), TARGET, intent(in) :: double_member
        
        INTEGER :: error ! Error flag
        LOGICAL :: avail
        INTEGER(HID_T) :: dcpl ! Handles
        INTEGER :: filter_info
        INTEGER :: filter_info_both
        INTEGER(HSIZE_T), DIMENSION(1) :: chunk ! chunk dimensions
        INTEGER(HID_T) :: dset_id       ! Dataset identifier
        INTEGER(HID_T) :: dspace_id     ! Dataspace identifier
        INTEGER(HSIZE_T), DIMENSION(1) :: data_dims
        INTEGER(HSIZE_T), DIMENSION(1) :: dims ! Dataset dimensions
        INTEGER :: dimsize ! Size of the dataset
        INTEGER(HID_T) :: h5_kind_type_i ! HDF type corresponding to the specified KIND
        TYPE(C_PTR) :: f_ptr
        
        dimsize = NNZ
        dims = (/dimsize/)
        data_dims(1) = dimsize
        
        !
        !  Check if gzip compression is available and can be used for both
        !  compression and decompression.  Normally we do not perform error
        !  checking in these examples for the sake of clarity, but in this
        !  case we will make an exception because this filter is an
        !  optional part of the hdf5 library.
        !
        CALL h5zfilter_avail_f(H5Z_FILTER_DEFLATE_F, avail, error)
        
        IF (.NOT.avail) THEN
            WRITE(*, '("gzip filter not available.",/)')
            STOP
        ENDIF
        CALL h5zget_filter_info_f(H5Z_FILTER_DEFLATE_F, filter_info, error)
        
        filter_info_both = IOR(H5Z_FILTER_ENCODE_ENABLED_F, H5Z_FILTER_DECODE_ENABLED_F)
        IF (filter_info /= filter_info_both) THEN
            WRITE(*, '("gzip filter not available for encoding and decoding.",/)')
            STOP
        ENDIF
        
        !
        ! Create the dataspace.
        !
        CALL h5screate_simple_f(rang, dims, dspace_id, error)
        !
        
        ! on ne compresse qu'à partir d'une certaine taille, et on utilise des bouts de taille différentes
        ! suvant la grosseur du tableau
        if ((dimsize > 500) .and. (dimsize < 100000)) then
            chunk = (/200/)
        elseif (dimsize >= 100000) then
            chunk = (/1000/)
        else
            chunk = (/0/) ! no chunk
        end if
        !
        ! Create the dataset creation property list, add the gzip
        ! compression filter and set the chunk size.
        !
        CALL h5pcreate_f(H5P_DATASET_CREATE_F, dcpl, error)
        if (chunk(1) > 0) then
            CALL h5pset_deflate_f(dcpl, 9, error)
            CALL h5pset_chunk_f(dcpl, rang, chunk, error)
        end if

        h5_kind_type_i = h5kind_to_type(int64,H5_INTEGER_KIND)
        ! Create the dataset with with default properties.
        !
        CALL h5dcreate_f(file_id, dsetname, h5_kind_type_i, dspace_id, &
                dset_id, error, dcpl)
        IF (error<0) THEN
            print *, __FILE__, ":", __LINE__, " *** Error creating dataset " // dsetname
            return
        else
#ifdef DEBUG
            print *, __FILE__, ":", __LINE__, " *** creating dataset " // dsetname
#endif
        ENDIF
        
        data_dims(1) = size(double_member)
        f_ptr = C_LOC(double_member(1))
        !
        ! Write data by fields in the datatype. Fields order is not important.
        !
        CALL h5dwrite_f(dset_id, h5_kind_type_i, f_ptr, error)
        IF (error<0) THEN
            print *, __FILE__, ":", __LINE__, " *** Error writing dataset " // dsetname
            return
        else
#ifdef DEBUG
            print *, __FILE__, ":", __LINE__, " *** writing file " // dsetname
            !print *, " data ", double_member
            print *, " data_dims ", data_dims
#endif
        ENDIF
        
        CALL h5pclose_f(dcpl, error)
        !
        ! End access to the dataset and release resources used by it.
        !
        CALL h5dclose_f(dset_id, error)
        
        !
        ! Terminate access to the data space.
        !
        CALL h5sclose_f(dspace_id, error)
    
    endsubroutine writeintbigdataset
    
    
    subroutine saveToHDF5(double_member, tab_dims, filename, dsetname)
        ! cette routine permet de sauver un tableau de double (real64) au format HDF5
        ! on passe toujours un tableau 1d en argument
        ! tab_dims contient autant de dimensions que d éléments du tableau
        ! par exemple: [3,2,3]: tableau 3D, [4, 8, 4, 6] tableau 4D
        ! ainsi que le nom du fichier et le nom du dataset à l intérieur du fichier HDF5
        ! le tableau doit être alloué avant l appel
        ! on stocke les dimensions de la matrice dans un deuxième dataset pour faciliter le post-traitement
        ! on utilise une compression gzip si le tableau dépasse une certaine taille
        real(kind = dp), DIMENSION(:), allocatable, intent(in) :: double_member
        character(len = *), intent(in) :: filename
        CHARACTER(LEN = *), intent(in) :: dsetname ! Dataset name
        INTEGER, DIMENSION(:), intent(in) :: tab_dims   !  he dataset
        
        INTEGER :: dimsize ! Size of the dataset
        
        INTEGER(HID_T) :: file_id       ! File identifier
        INTEGER(HID_T) :: dset_id       ! Dataset identifier
        INTEGER(HID_T) :: dspace_id     ! Dataspace identifier
        
        !INTEGER(HSIZE_T), DIMENSION(1) :: dims = (/dimsize/) ! Dataset dimensions
        INTEGER(HSIZE_T), DIMENSION(1) :: dims ! Dataset dimensions
        INTEGER(HSIZE_T), DIMENSION(1) :: chunk ! chunk dimensions
        INTEGER :: rang = 1                            ! Dataset rang
        
        INTEGER :: error ! Error flag
        INTEGER(HSIZE_T), DIMENSION(1) :: data_dims
        
        CHARACTER(LEN = *), parameter :: dsetname2 = "/dimensions"
        integer, dimension(3) :: data2 ! pr stocker les 3 dimensions de la matrice dans un dataset
        
        LOGICAL :: avail
        INTEGER(HID_T) :: dcpl ! Handles
        INTEGER :: filter_info
        INTEGER :: filter_info_both
        
        select case (size(tab_dims))
        case (1)
            dimsize = tab_dims(1)
        case (2)
            dimsize = tab_dims(1) * tab_dims(2)
        case (3)
            dimsize = tab_dims(1) * tab_dims(2) * tab_dims(3)
        case (4)
            dimsize = tab_dims(1) * tab_dims(2) * tab_dims(3)
            dimsize = dimsize * tab_dims(4)
        case default
            WRITE(*, '("Error rang is > 4 not available.",/)')
            STOP
        end select
        dims = (/dimsize/)
        data_dims(1) = dimsize
        !
        ! Initialize FORTRAN interface.
        !
        CALL h5open_f(error)
        !
        
        !
        !  Check if gzip compression is available and can be used for both
        !  compression and decompression.  Normally we do not perform error
        !  checking in these examples for the sake of clarity, but in this
        !  case we will make an exception because this filter is an
        !  optional part of the hdf5 library.
        !
        CALL h5zfilter_avail_f(H5Z_FILTER_DEFLATE_F, avail, error)
        
        IF (.NOT.avail) THEN
            WRITE(*, '("gzip filter not available.",/)')
            STOP
        ENDIF
        CALL h5zget_filter_info_f(H5Z_FILTER_DEFLATE_F, filter_info, error)
        
        filter_info_both = IOR(H5Z_FILTER_ENCODE_ENABLED_F, H5Z_FILTER_DECODE_ENABLED_F)
        IF (filter_info /= filter_info_both) THEN
            WRITE(*, '("gzip filter not available for encoding and decoding.",/)')
            STOP
        ENDIF
        
        !
        ! Create a new file using default properties.
        !
        CALL h5fcreate_f(filename, H5F_ACC_TRUNC_F, file_id, error)
        
        !
        ! Create the dataspace.
        !
        CALL h5screate_simple_f(rang, dims, dspace_id, error)
        !
        
        ! on ne compresse qu'à partir d'une certaine taille, et on utilise des bouts de taille différentes
        ! suvant la grosseur du tableau
        if ((dimsize > 500) .and. (dimsize < 100000)) then
            chunk = (/200/)
        elseif (dimsize >= 100000) then
            chunk = (/1000/)
        else
            chunk = (/0/) ! no chunk
        end if
        !
        ! Create the dataset creation property list, add the gzip
        ! compression filter and set the chunk size.
        !
        CALL h5pcreate_f(H5P_DATASET_CREATE_F, dcpl, error)
        if (chunk(1) > 0) then
            CALL h5pset_deflate_f(dcpl, 9, error)
            CALL h5pset_chunk_f(dcpl, rang, chunk, error)
        end if
        ! Create the dataset with with default properties.
        !
        CALL h5dcreate_f(file_id, dsetname, h5kind_to_type(dp, H5_REAL_KIND), dspace_id, &
                dset_id, error)
        
        !
        ! Write data by fields in the datatype. Fields order is not important.
        !
        CALL h5dwrite_f(dset_id, h5kind_to_type(dp, H5_REAL_KIND), double_member, data_dims, error)
        IF (error<0) THEN
            print *, __FILE__, ":", __LINE__, " *** Error writing dataset " // dsetname
            return
        else
#ifdef DEBUG
            print *, __FILE__, ":", __LINE__, " *** writing file " // dsetname
            !print *, " data ", double_member
            print *, " data_dims ", data_dims
#endif
        
        ENDIF
        
        CALL h5pclose_f(dcpl, error)
        !
        ! End access to the dataset and release resources used by it.
        !
        CALL h5dclose_f(dset_id, error)
        
        !
        ! Terminate access to the data space.
        !
        CALL h5sclose_f(dspace_id, error)
        
        ! on crée un nouveau dataset pour stocker les dimensions de la matrice
        dims = (size(tab_dims))
        data_dims(1) = size(tab_dims)
        CALL h5screate_simple_f(rang, dims, dspace_id, error)
        CALL h5dcreate_f(file_id, dsetname2, H5T_NATIVE_INTEGER, dspace_id, &
                dset_id, error)
        CALL h5dwrite_f(dset_id, H5T_NATIVE_INTEGER, tab_dims, data_dims, error)
        CALL h5dclose_f(dset_id, error)
        
        !
        ! Close the file.
        !
        CALL h5fclose_f(file_id, error)
    
    end subroutine saveToHDF5
    
    ! la taille du tableau data_out n'est connu qu'après avoir lu les attributs du fichier hdf5
    ! idem pour la taille du tableau tab_dims
    ! il y a une particularité qui oblige à définir la précision avec real64, prce que sinon, on ne peut pas
    ! relire correctement un fichier venant de Matlab!
    ! voir par exemple https://stackoverflow.com/questions/43639836/reading-array-of-floats-from-hdf5-file-in-fortran
    ! ou bien https://fortran-lang.discourse.group/t/best-way-to-declare-a-double-precision-in-fortran/69/2
    subroutine readHDF5(filename, dsetname, data_out, tab_dims)
        CHARACTER(LEN = *), intent(in) :: filename
        CHARACTER(LEN = *), intent(in) :: dsetname
        DOUBLE PRECISION, DIMENSION(:), allocatable, intent(out) :: data_out
        INTEGER, DIMENSION(:), allocatable, intent(out) :: tab_dims   !  he dataset
        
        INTEGER(HID_T) :: file_id       ! File identifier
        INTEGER(HID_T) :: dset_id       ! Dataset identifier
        INTEGER(HID_T) :: dspace_id       ! Dataspace identifier
        
        INTEGER :: error ! Error flag
        INTEGER :: i, j
        
        INTEGER(HSIZE_T), DIMENSION(2) :: data_dims
        CHARACTER(LEN = *), PARAMETER :: dsetname2 = "dimensions"
        integer(hsize_t), dimension(:), allocatable :: dims, maxdims
        real(dp), dimension(:, :), allocatable :: tab2d  ! on aurait aussi pu écrire avec real64
        real(dp), dimension(:, :, :), allocatable :: tab3d
        real(dp), dimension(:, :, :, :), allocatable :: tab4d
        integer :: rang
        !
        ! Initialize FORTRAN interface.
        !
        CALL h5open_f(error)
        
        !
        ! Open an existing file.
        !
        CALL h5fopen_f (filename, H5F_ACC_RDONLY_F, file_id, error)
        IF (error<0) THEN
            print *, __FILE__, ":", __LINE__, " *** Error opening file " // filename
            return
        ENDIF
        
        !
        ! on regarde si le dataset des dimensions existe, ce n est pas obligé
        !
        CALL h5dopen_f(file_id, dsetname2, dset_id, error)
        IF (error<0) THEN
            print *, __FILE__, ":", __LINE__, " ", " *** Error opening dataset " // dsetname2
            return
        else
#ifdef DEBUG
            print *, __FILE__, ":", __LINE__, " ", " opening dataset " // dsetname2
#endif
            ! Get the dataspace ID
            call h5dget_space_f(dset_id, dspace_id, error)
            
            ! Getting dims from dataspace
            call h5sget_simple_extent_ndims_f(dspace_id, rang, error)
#ifdef DEBUG
            print *, __FILE__, ":", __LINE__, " ", " rank ", rang
#endif
            allocate(dims(rang))
            allocate(maxdims(rang))
            call h5sget_simple_extent_dims_f(dspace_id, dims, maxdims, error)
#ifdef DEBUG
            print *, __FILE__, ":", __LINE__, " ", " dims ", dims, " maxdims ", maxdims
#endif
            
            ! Allocate memory for the array.
            allocate(tab_dims(dims(1)))
            ! ----------------
            ! Read the array.
            CALL h5dread_f(dset_id, H5T_NATIVE_INTEGER, tab_dims, dims, error)
            IF (error<0) THEN
                print *, __FILE__, ":", __LINE__, " ", " *** Error reading  dataset " // dsetname2
                return
            ENDIF
#ifdef DEBUG
            print *, __FILE__, ":", __LINE__, " ", " tab_dims ", tab_dims
#endif
            call h5sclose_f(dspace_id, error)
            call h5dclose_f(dset_id, error)
            deallocate(dims)
            deallocate(maxdims)
        ENDIF
        
        !
        ! Open an existing dataset.
        !
        ! Maintenant on va lire le dataset des données après avoir récupéré la taille
        CALL h5dopen_f(file_id, dsetname, dset_id, error)
        IF (error<0) THEN
            print *, __FILE__, ":", __LINE__, " ", " *** Error opening dataset " // dsetname
            return
        else
#ifdef DEBUG
            print *, __FILE__, ":", __LINE__, " ", " opening dataset " // dsetname
#endif
        ENDIF
        call h5dget_space_f(dset_id, dspace_id, error)
        call h5sget_simple_extent_ndims_f(dspace_id, rang, error)
#ifdef DEBUG
        print *, __FILE__, ":", __LINE__, " ", " rang ", rang
#endif
        allocate(dims(rang))
        allocate(maxdims(rang))
        call h5sget_simple_extent_dims_f(dspace_id, dims, maxdims, error)
#ifdef DEBUG
        print *, __FILE__, ":", __LINE__, " ", " dims ", dims, " maxdims ", maxdims
#endif
        select case (rang)
        case (1)
            allocate(data_out(dims(1)))
            CALL h5dread_f(dset_id, H5T_IEEE_F64LE, data_out, dims, error)
        case (2)
            allocate(tab2d(dims(1), dims(2)))
            CALL h5dread_f(dset_id, h5kind_to_type(dp, H5_REAL_KIND), tab2d, dims, error)
            data_out = pack(tab2d, mask = .true.)
            deallocate(tab2d)
        case (3)
            allocate(tab3d(dims(1), dims(2), dims(3)))
            CALL h5dread_f(dset_id, H5T_IEEE_F64LE, tab3d, dims, error)
            data_out = pack(tab3d, mask = .true.)
            deallocate(tab3d)
        case (4)
            allocate(tab4d(dims(1), dims(2), dims(3), dims(4)))
            CALL h5dread_f(dset_id, H5T_IEEE_F64LE, tab4d, dims, error)
            data_out = pack(tab4d, mask = .true.)
            deallocate(tab4d)
        case default
            print *, __FILE__, ":", __LINE__, " ", " *** Error rang ", rang, " missing code "
            return
        end select
        deallocate(dims)
        deallocate(maxdims)
        
        !
        ! Close the dataset.
        !
        CALL h5dclose_f(dset_id, error)
        
        !
        ! Close the file.
        !
        CALL h5fclose_f(file_id, error)
        
        !
        ! Close FORTRAN interface.
        !
        CALL h5close_f(error)
    end subroutine readHDF5
    
    ! routine pour sauver une matrice utilisée par Herve Tortel
    ! N
    ! NNZ
    ! row col real imag
    ! on stocke donc 5 tableaux 1d dans 5 datasets
    subroutine saveGlobalMatrixToHDF5(file_id, N, NNZ, tab_rows, tab_cols, re_mat, im_mat)
        ! cette routine permet de sauver un tableau de double (real64) au format HDF5
        ! on passe toujours un tableau 1d en argument
        ! les 4 tableaux 1D ont pour taille NNZ
        ! on utilise une compression gzip si le tableau dépasse une certaine taille
        INTEGER(HID_T) :: file_id       ! File identifier
        integer, intent(in) :: N, NNZ
        real(kind = dp), DIMENSION(:), allocatable, intent(in) :: re_mat, im_mat

        CHARACTER(LEN = *), parameter :: dset_dims = "/dimensions", dset_row = "/rows", dset_col = "/cols", &
                &dset_re_mat = "/re_mat", dset_im_mat = "/im_mat" !
        INTEGER(kind=int64), DIMENSION(:), intent(in) :: tab_rows, tab_cols   !  he dataset
        INTEGER :: dimsize ! Size of the dataset
        

        INTEGER(HID_T) :: dset_id       ! Dataset identifier
        INTEGER(HID_T) :: dspace_id     ! Dataspace identifier
        
        !INTEGER(HSIZE_T), DIMENSION(1) :: dims = (/dimsize/) ! Dataset dimensions
        INTEGER(HSIZE_T), DIMENSION(1) :: dims ! Dataset dimensions
        INTEGER :: rang = 1                            ! Dataset rang
        
        INTEGER :: error ! Error flag
        INTEGER(HSIZE_T), DIMENSION(1) :: data_dims
        
        integer, dimension(2) :: data2 ! pr stocker N et NNZ
        
        ! on crée un dataset pour stocker N et NNZ
        dims = (/2/)
        data2 = (/N, NNZ/)
        CALL h5screate_simple_f(rang, dims, dspace_id, error)
        CALL h5dcreate_f(file_id, dset_dims, H5T_NATIVE_INTEGER, dspace_id, &
                dset_id, error)
        CALL h5dwrite_f(dset_id, H5T_NATIVE_INTEGER, data2, dims, error)
        CALL h5dclose_f(dset_id, error)
        
!        ! on ajoute les attributs au dataset à partir de ce qu'on trouve dans le fichier nomfich
!        print *, "donnez le nom du fichier contenant les commentaires à ajouter (p.e.attr_simu.txt): "
!        read *, nomfich
!        call add_attribute(file_id, dset_dims, trim(nomfich))
        
        call writeintbigdataset(file_id, NNZ, dset_row, rang, tab_rows)
        call writeintbigdataset(file_id, NNZ, dset_col, rang, tab_cols)
        call writerealbigdataset(file_id, NNZ, dset_re_mat, rang, re_mat)
        call writerealbigdataset(file_id, NNZ, dset_im_mat, rang, im_mat)
    
    end subroutine saveGlobalMatrixToHDF5
    
    subroutine readrealdataset(file_id, dsetname, data_out, error)
        INTEGER(HID_T) :: file_id       ! File identifier
        CHARACTER(LEN = *), intent(in) :: dsetname
        integer(hsize_t), dimension(:), allocatable :: dims, maxdims
        DOUBLE PRECISION, DIMENSION(:), allocatable, intent(out) :: data_out
        INTEGER(HID_T) :: dset_id       ! Dataset identifier
        INTEGER(HID_T) :: dspace_id       ! Dataspace identifier
        integer :: rang
        
        INTEGER :: error ! Error flag
        
        CALL h5dopen_f(file_id, dsetname, dset_id, error)
        IF (error<0) THEN
            print *, __FILE__, ":", __LINE__, " ", " *** Error opening dataset " // dsetname
            return
        else
#ifdef DEBUG
            print *, __FILE__, ":", __LINE__, " ", " opening dataset " // dsetname
#endif
        ENDIF
        call h5dget_space_f(dset_id, dspace_id, error)
        IF (error<0) THEN
            print *, __FILE__, ":", __LINE__, " ", " *** Error get_space " // dsetname
            return
        ENDIF
        call h5sget_simple_extent_ndims_f(dspace_id, rang, error)
        IF (error<0) THEN
            print *, __FILE__, ":", __LINE__, " ", " *** Error get_simple_extent_ndims " // dsetname
            return
        else
#ifdef DEBUG
        print *, __FILE__, ":", __LINE__, " ", " rang ", rang
#endif
        ENDIF
        allocate(dims(rang))
        allocate(maxdims(rang))
        call h5sget_simple_extent_dims_f(dspace_id, dims, maxdims, error)
        IF (error<0) THEN
            print *, __FILE__, ":", __LINE__, " ", " *** Error get_simple_extent_dims " // dsetname
            return
        else
#ifdef DEBUG
        print *, __FILE__, ":", __LINE__, " ", " dims ", dims, " maxdims ", maxdims
#endif
        ENDIF
        allocate(data_out(dims(1)))
        CALL h5dread_f(dset_id, H5T_IEEE_F64LE, data_out, dims, error)
        IF (error<0) THEN
            print *, __FILE__, ":", __LINE__, " ", " *** Error read " // dsetname
            return
        ENDIF
        deallocate(dims)
        deallocate(maxdims)
        !
        ! Close the dataset.
        !
        CALL h5dclose_f(dset_id, error)
    
    end subroutine readrealdataset
    
    subroutine readintdataset(file_id, dsetname, data_out, error)
        INTEGER(HID_T) :: file_id       ! File identifier
        CHARACTER(LEN = *), intent(in) :: dsetname
        integer(hsize_t), dimension(:), allocatable :: dims, maxdims
        INTEGER(kind=int64), DIMENSION(:), allocatable, TARGET, intent(out) :: data_out
        INTEGER(HID_T) :: dset_id       ! Dataset identifier
        INTEGER(HID_T) :: dspace_id       ! Dataspace identifier
        integer :: rang
        INTEGER(HID_T) :: h5_kind_type_i ! HDF type corresponding to the specified KIND
        TYPE(C_PTR) :: f_ptr
        
        INTEGER :: error ! Error flag
        
        CALL h5dopen_f(file_id, dsetname, dset_id, error)
        IF (error<0) THEN
            print *, __FILE__, ":", __LINE__, " ", " *** Error opening dataset " // dsetname
            return
        else
#ifdef DEBUG
            print *, __FILE__, ":", __LINE__, " ", " opening dataset " // dsetname
#endif
        ENDIF
        call h5dget_space_f(dset_id, dspace_id, error)
        IF (error<0) THEN
            print *, __FILE__, ":", __LINE__, " ", " *** Error get_space " // dsetname
            return
        ENDIF
        call h5sget_simple_extent_ndims_f(dspace_id, rang, error)
        IF (error<0) THEN
            print *, __FILE__, ":", __LINE__, " ", " *** Error get_simple_extent_ndims " // dsetname
            return
        else
#ifdef DEBUG
        print *, __FILE__, ":", __LINE__, " ", " rang ", rang
#endif
        ENDIF
        allocate(dims(rang))
        allocate(maxdims(rang))
        call h5sget_simple_extent_dims_f(dspace_id, dims, maxdims, error)
        IF (error<0) THEN
            print *, __FILE__, ":", __LINE__, " ", " *** Error get_simple_extent_dims " // dsetname
            return
        else
#ifdef DEBUG
        print *, __FILE__, ":", __LINE__, " ", " dims ", dims, " maxdims ", maxdims
#endif
        ENDIF
        allocate(data_out(dims(1)))
        f_ptr = C_LOC(data_out(1))
        h5_kind_type_i = h5kind_to_type(int64,H5_INTEGER_KIND)
        CALL h5dread_f(dset_id, h5_kind_type_i, f_ptr, error)
        IF (error<0) THEN
            print *, __FILE__, ":", __LINE__, " ", " *** Error read " // dsetname
            return
        ENDIF
        deallocate(dims)
        deallocate(maxdims)
        !
        ! Close the dataset.
        !
        CALL h5dclose_f(dset_id, error)
    
    end subroutine readintdataset
    
    
    subroutine readglobalmatrixhdf5(file_id, N, NNZ, tab_rows, tab_cols, tab_re_mat, tab_im_mat, error)
        INTEGER(HID_T) :: file_id       ! File identifier
        INTEGER, intent(out) :: N, NNZ
        INTEGER(kind=int64), DIMENSION(:), allocatable, intent(out) :: tab_rows, tab_cols
        DOUBLE PRECISION, DIMENSION(:), allocatable, intent(out) :: tab_re_mat, tab_im_mat
        
        INTEGER :: error ! Error flag

        INTEGER(HID_T) :: dset_id       ! Dataset identifier
        INTEGER(HID_T) :: dspace_id       ! Dataspace identifier
        CHARACTER(LEN = *), parameter :: dset_dims = "/dimensions", dset_row = "/rows", dset_col = "/cols", &
                &dset_re_mat = "/re_mat", dset_im_mat = "/im_mat" !
        integer(hsize_t), dimension(:), allocatable :: dims, maxdims
        integer :: rang
        INTEGER, DIMENSION(:), allocatable :: tab_dims   !  he dataset
        
        print *, __FILE__, ":", __LINE__, " debut "
        error = 0 ! pas d erreur par défaut
        
        rang = 1
        
        CALL h5dopen_f(file_id, "dimensions", dset_id, error)
        allocate(dims(rang))
        dims(1) = 2
        allocate(tab_dims(dims(1)))
        CALL h5dread_f(dset_id, H5T_NATIVE_INTEGER, tab_dims, dims, error)
        !print *, "COUCOU", tab_dims, dims
        N = tab_dims(1)
        NNZ = tab_dims(2)
        call h5dclose_f(dset_id, error)
        deallocate(dims)
        deallocate(tab_dims)
        
        call readintdataset(file_id, dset_row, tab_rows, error)
        IF (error<0) THEN
            print *, __FILE__, ":", __LINE__, " *** Error reading dataset " // dset_row
            return
        else
#ifdef DEBUG
        print *, __FILE__, ":", __LINE__, " *** End reading dataset " // dset_row
#endif
        ENDIF
        call readintdataset(file_id, dset_col, tab_cols, error)
        IF (error<0) THEN
            print *, __FILE__, ":", __LINE__, " *** Error reading dataset " // dset_col
            return
        else
#ifdef DEBUG
        print *, __FILE__, ":", __LINE__, " *** End reading dataset " // dset_col
#endif
        ENDIF
        call readrealdataset(file_id, dset_re_mat, tab_re_mat, error)
        IF (error<0) THEN
            print *, __FILE__, ":", __LINE__, " *** Error reading dataset " // dset_re_mat
            return
        else
#ifdef DEBUG
        print *, __FILE__, ":", __LINE__, " *** End reading dataset " // dset_re_mat
#endif
        ENDIF
        call readrealdataset(file_id, dset_im_mat, tab_im_mat, error)
        IF (error<0) THEN
            print *, __FILE__, ":", __LINE__, " *** Error reading dataset " // dset_im_mat
            return
        else
#ifdef DEBUG
        print *, __FILE__, ":", __LINE__, " *** End reading dataset " // dset_im_mat
#endif
        ENDIF
    
    end subroutine readglobalmatrixhdf5

end module module_hdf5
