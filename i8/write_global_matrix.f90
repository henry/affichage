program write_global_matrix
    use, intrinsic :: iso_c_binding
    use iso_fortran_env, only : int64
    use module_hdf5
    use hdf5_wrapper
    implicit none
    integer :: ix, iy, iz, kkk
    REAL*8 RE_MAT, IM_MAT, RE_RHS, IM_RHS
    INTEGER(kind = int64), DIMENSION(:), allocatable :: tab_rows, tab_cols
    DOUBLE PRECISION, DIMENSION(:), allocatable :: double_member, tab_re_mat, tab_im_mat
    integer :: ok
    CHARACTER(LEN = *), parameter :: filename = "MATRIX_STORAGE/global_matrix.h5"
    CHARACTER(LEN = *), parameter :: filemtx = "MATRIX_STORAGE/global_matrix.mtx"
    integer :: N, NNZ
    integer(kind = int64) :: I8, ROW, COL
    integer(kind = int64) :: i1, i2
    integer :: i3, i4
    integer(kind = int64) :: i5
    double precision :: frequency
    
    integer(hid_t) :: ifile
    character ofile*32
    integer :: ounit

#ifdef DEBUG_INT64
! voir https://fortran-lang.discourse.group/t/does-64-bit-integer-literals-need-a-suffix/2029/3
    print *, "test taille des entiers"
    i1 = 1234567890123_int64
    i2 = 1234567890123
    i3 = 1234567890123_int64
    i4 = 1234567890123
    print*,i1,i2,i3,i4
    i1 = 123456_int64
    i2 = 123456
    i3 = 123456_int64
    i4 = 123456
    print*,i1,i2,i3,i4
    print *, "fin test taille des entiers"
    print *
    
    STOP 1
#endif
    !    character (len=50) :: nomfich ! fichier contenant éventuellement des attributs à ajouter au dataset dimensions
    !    i5 = 1
    !    read *, nomfich
    !    call add_attribute(i5, "dimension", trim(nomfich))
    !    stop 1
    !
    !
    !
    
    ! on commence par ajouter les attributs à un fichier vide
    call hdf5_init()
    call hdf5_create_file(filename)
    call hdf5_open_file(filename, ifile)
    call hdf5_write_data(ifile, '/description', 4)  ! 3 pour dire qu'on a mis 3 attributs
    frequency = 1.0e9
    call hdf5_write_attribute(ifile, '/description', 'attr_name1', 1.23)
    call hdf5_write_attribute(ifile, '/description', 'attr_name2', 'EF_3D_NEW')
    call hdf5_write_attribute(ifile, '/description', 'contexte', 'petite matrice')
    call hdf5_write_attribute(ifile, '/description', 'provider', 'tortel')
    
    open(unit = 123, file = "TEMP_FILE/global_matrix.dat")
    READ(123, *) N
    READ(123, *) NNZ
    
    ! on sauve un tableau 1d
    print *, "  lecture fichiers global_matrix et rhs"
    allocate(tab_re_mat(NNZ), stat = ok)
    if (ok /= 0) then
        print *, "Erreur tab_re_mat allocation impossible ", NNZ
        stop 124
    end if
    allocate(tab_im_mat(NNZ), stat = ok)
    if (ok /= 0) then
        print *, "Erreur tab_im_mat allocation impossible ", NNZ
        stop 125
    end if
    allocate(tab_cols(NNZ), stat = ok)
    if (ok /= 0) then
        print *, "Erreur tab_cols allocation impossible ", NNZ
        stop 126
    end if
    allocate(tab_rows(NNZ), stat = ok)
    if (ok /= 0) then
        print *, "Erreur tab_rows allocation impossible ", NNZ
        stop 126
    end if
    DO I8 = 1, NNZ
        READ(123, *) row, col, re_mat, im_mat
        tab_rows(I8) = row
        tab_cols(I8) = col
        tab_re_mat(I8) = re_mat
        tab_im_mat(I8) = im_mat
    END DO
    close(123)
    CALL saveGlobalMatrixToHDF5(ifile, N, NNZ, tab_rows, tab_cols, tab_re_mat, tab_im_mat)
    call hdf5_close_file(ifile)
    call hdf5_finalize()
    
    if (iargc() .gt. 0) then
        call getarg(1, ofile)
        print *, "on ecrit la matrice au format matrix market"
        ounit = 9
        open(unit = ounit, file = filemtx)
        print *, 'Writing header and data...'
        write(ounit, '(A)') "%%MatrixMarket matrix coordinate  complex     general"
        write(ounit, *) N, N, NNZ
        DO I8 = 1, NNZ
            write(ounit, *) tab_rows(I8), tab_cols(I8), tab_re_mat(I8), tab_im_mat(I8)
        END DO
        close(9)
        print *, 'Done writing to file: ', filemtx
    else
        print *, 'si vous voulez du MatrixMarket, ajouter le nom de fichier en second argument'
    endif
    
    deallocate(tab_rows)
    deallocate(tab_cols)
    deallocate(tab_re_mat)
    deallocate(tab_im_mat)

end program write_global_matrix

