! Created by me on 15/05/23.

program read_global_matrix
    use iso_fortran_env, only : int32, int64
    use, intrinsic :: iso_c_binding
    use hdf5_wrapper
    use module_hdf5
    
    implicit none
    integer :: ix, iy, iz, nx, ny, nz
    !real  :: f(10,10,10)
    integer :: ii, ndims
    
    INTEGER(kind = int64), DIMENSION(:), allocatable :: tab_rows, tab_cols
    DOUBLE PRECISION, DIMENSION(:), allocatable :: re_mat, im_mat
    integer :: N, NNZ
    integer :: i, error
    character (len = 255) :: matrice, ofile
    integer(hid_t) :: ifile
    integer :: ounit
    logical :: file_exists, dataset_exists, attribute_exists
    character(len = 100) :: y
    character(len = 100), allocatable :: list(:)
    
    if (iargc() .gt. 0) then
        call getarg(1, matrice)
    else
        stop "missing hdf5 file"
    end if
    
    INQUIRE(FILE = trim(matrice), EXIST = file_exists)
    if (file_exists .eqv. .false.) then
        stop "error file"
    else
        print *, trim(matrice)
    end if
    
    call hdf5_init()
    call hdf5_open_file(trim(matrice), ifile, rdonly = .true.)
    ! we provide an allocatable array of characters
    !    write(*,*) 'groups in / ---'
    !    call hdf5_list_groups(ifile, '/', list)
    !    do i = 1, size(list)
    !        write(*,*) trim(list(i))
    !    enddo
    !    deallocate(list)
    !    write(*,*) '--- groups'
    write(*, *)
    write(*, *) 'dataset in / ---'
    call hdf5_list_datasets(ifile, '/', list)
    do i = 1, size(list)
        write(*, *) trim(list(i))
    enddo
    deallocate(list)
    write(*, *) '--- datasets'
    
    dataset_exists = hdf5_dataset_exists(ifile, '/description')
    write(*, *) dataset_exists
    if (dataset_exists  .eqv. .false.) then
        stop "error file does not contain group /description. See https://gitlab.fresnel.fr/hipe/stage2022"
    end if
    attribute_exists = hdf5_attribute_exists(ifile, '/description', 'attr_name2')
    if (attribute_exists  .eqv. .false.) then
        stop "error file does not contain correct attribute. See https://gitlab.fresnel.fr/hipe/stage2022"
    end if
    call hdf5_read_attribute(ifile, '/description', 'attr_name2', y)
    write(*, *) ' attribut ', trim(y), " trouve, on peut continuer"
    if (y /= 'EF_3D_NEW') then
        print *, "on trouve: ****", y, "****"
        stop "error file does not relate to EF_3D_NEW. See https://gitlab.fresnel.fr/hipe/stage2022"
    end if
    
    call readGlobalMatrixHDF5(ifile, N, NNZ, tab_rows, tab_cols, re_mat, im_mat, error)
    if (error /= 0) then
        print *, "Erreur de traitement du fichier hdf5"
        stop 1
    end if
    call hdf5_close_file(ifile)
    call hdf5_finalize()
    
    print *, N, NNZ
    
    print *, "Nouveau: ecriture au format MatrixMarket (voir http://math.nist.gov/MatrixMarket/)"
    ofile = "global_matrix.mtx"
    ounit = 9
    open(unit = ounit, file = ofile)
    print *, 'Writing header and data...'
    write(ounit, '(A)') "%%MatrixMarket matrix coordinate  complex     general"
    write(ounit, *) N, N, NNZ
    do ix = 1, NNZ
        write(unit = ounit, fmt = *) tab_rows(ix), tab_cols(ix), re_mat(ix), im_mat(ix)
    enddo
    close(ounit)
    print *, 'Done writing to file: ', ofile
    
    deallocate(tab_rows)
    deallocate(tab_cols)
    deallocate(re_mat)
    deallocate(im_mat)
end program read_global_matrix
